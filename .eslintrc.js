module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-typescript',
  ],
  parserOptions: {
    project: ["tsconfig.eslint.json"]
  },
  rules: {
    "@typescript-eslint/lines-between-class-members": [ 
      'error',
      'always',
      { 'exceptAfterSingleLine': true },
    ],
    "no-plusplus": ["error", {allowForLoopAfterthoughts: true}],
    "class-methods-use-this": "off",
    "import/prefer-default-export" : "off",
    "linebreak-style": "off",
  },
};
