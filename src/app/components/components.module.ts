import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
    NavComponent,
  ],
  exports: [
    NavComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ],
})
export class ComponentsModule { }
