import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatabaseService } from './database.service';

export function initDb(db: DatabaseService) {
  return () => db.init();
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initDb,
      deps: [DatabaseService],
      multi: true,
    },
    DatabaseService,
  ],
})
export class DbModule { }
