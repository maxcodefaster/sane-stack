import { RxGraphQLReplicationQueryBuilder, RxJsonSchema } from 'rxdb/dist/types/core';

export interface DBSchema {
  authRequired: boolean;
  batchSize: number;
  schema: RxJsonSchema;
  pullQuery: RxGraphQLReplicationQueryBuilder;
  pushQuery: RxGraphQLReplicationQueryBuilder;
}
