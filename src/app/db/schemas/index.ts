import { DBSchema } from './DBSchema';
import {
  TodoSchema,
} from './todo.schema';

export const dbSchemaIndex: DBSchema[] = [
  new TodoSchema(),
];
