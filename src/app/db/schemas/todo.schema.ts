import type {
  RxJsonSchema,
} from 'rxdb/plugins/core';
import { RxGraphQLReplicationQueryBuilder } from 'rxdb/dist/types/core';
import { DBSchema } from './DBSchema';

export class TodoSchema implements DBSchema {
  authRequired = false;
  batchSize = 5;

  schema: RxJsonSchema = {
    title: 'todoschema',
    description: 'todo schema',
    version: 0,
    type: 'object',
    properties: {
      id: {
        type: 'string',
        primary: true,
      },
      text: {
        type: 'string',
      },
      isCompleted: {
        type: 'boolean',
      },
      createdAt: {
        type: 'string',
        format: 'date-time',
      },
      updatedAt: {
        type: 'string',
        format: 'date-time',
      },
      userId: {
        type: 'string',
      },
    },
    required: ['text', 'isCompleted', 'userId', 'createdAt'],
  };

  pullQuery: RxGraphQLReplicationQueryBuilder = (doc) => {
    if (!doc) {
      // eslint-disable-next-line no-param-reassign
      doc = {
        id: '',
        updatedAt: new Date(0).toUTCString(),
      };
    }

    const query = `{
            todos(
                where: {
                    _or: [
                        {updatedAt: {_gt: "${doc.updatedAt}"}},
                        {
                            updatedAt: {_eq: "${doc.updatedAt}"},
                            id: {_gt: "${doc.id}"}
                        }
                    ],
                    userId: {_eq: "${doc.userId}"}
                },
                limit: ${this.batchSize},
                order_by: [{updatedAt: asc}, {id: asc}]
            ) {
                id
                text
                isCompleted
                deleted
                createdAt
                updatedAt
                userId
            }
        }`;
    return {
      query,
      variables: {},
    };
  };

  pushQuery: RxGraphQLReplicationQueryBuilder = (doc) => {
    const query = `
 mutation InsertTodo($todo: [todos_insert_input!]!) {
            insert_todos(
                objects: $todo,
                on_conflict: {
                    constraint: todos_pkey,
                    update_columns: [text, isCompleted, deleted, updatedAt]
                }){
                returning {
                  id
                }
              }
       }
`;
    const variables = {
      [this.schema.title]: doc,
    };

    return {
      query,
      variables,
    };
  };
}
