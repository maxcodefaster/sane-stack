import PouchdbAdapterIdb from 'pouchdb-adapter-idb';
import PouchdbAdapterSQLite from 'pouchdb-adapter-cordova-sqlite';
import {
  RxDatabase, createRxDatabase, addRxPlugin, RxCollection,
} from 'rxdb/plugins/core';
import { RxDBNoValidatePlugin } from 'rxdb/plugins/no-validate';
import {
  RxDBReplicationGraphQLPlugin,
  RxGraphQLReplicationState,
} from 'rxdb/plugins/replication-graphql';
import { RxDBReplicationPlugin } from 'rxdb/plugins/replication';
import { RxDBUpdatePlugin } from 'rxdb/plugins/update';
import { Injectable, isDevMode } from '@angular/core';
import { Platform } from '@ionic/angular';
import { dbSchemaIndex } from './schemas';
import { HbpService } from '../shared/services/hbp.service';
import { DBSchema } from './schemas/DBSchema';

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  DB_INSTANCES: RxDatabase[] = [];
  replicationState: RxGraphQLReplicationState[] = [];

  readonly syncURL = 'http://localhost:8080/v1/graphql';

  get dbInstances(): RxDatabase[] {
    return this.DB_INSTANCES;
  }

  collection(dbName: string): RxCollection {
    return this.DB_INSTANCES.find((db: RxDatabase) => db.name === dbName)[dbName];
  }

  constructor(
    private plt: Platform,
    private hbp: HbpService,
  ) {
  }

  async init() {
    const adapter = this.plt.is('hybrid') ? 'cordova-sqlite' : 'idb';
    await this.loadRxDBPlugins(adapter);

    dbSchemaIndex.forEach((dbSchema: DBSchema, index: number) => {
      this.create(adapter, dbSchema).then((db: RxDatabase) => {
        this.DB_INSTANCES.push(db);
        this.startSync(dbSchema, db, index);
      });
    });
  }

  async create(adapter: string, dbSchema: DBSchema): Promise<RxDatabase> {
    const db = await createRxDatabase({
      name: dbSchema.schema.title,
      adapter,
      // password: '|8S_~|nC1>Vf&-9',
      pouchSettings: {
        revs_limit: 10,
        auto_compaction: true,
      },
    });

    await db.collection({
      name: dbSchema.schema.title,
      schema: dbSchema.schema,
    });

    return db;
  }

  async startSync(dbSchema: DBSchema, db: RxDatabase, i :number) {
    if (this.replicationState[i]) await this.replicationState[i].cancel();

    const headers = dbSchema.authRequired ? { Authorization: `Bearer ${this.hbp.jwt}` } : {};

    this.replicationState[i] = db[db.name].syncGraphQL({
      url: this.syncURL,
      headers,
      push: {
        batchSize: dbSchema.batchSize,
        queryBuilder: dbSchema.pushQuery,
      },
      pull: {
        queryBuilder: dbSchema.pullQuery,
      },
      live: true,
      liveInterval: 1000 * 15, // 15 seconds
      deletedFlag: 'deleted',
    });
  }

  async loadRxDBPlugins(adapter: string): Promise<void> {
    addRxPlugin(RxDBReplicationPlugin);
    addRxPlugin(RxDBReplicationGraphQLPlugin);
    addRxPlugin(RxDBUpdatePlugin);

    if (adapter === 'cordova-sqlite') {
      addRxPlugin(PouchdbAdapterSQLite);
    } else {
      addRxPlugin(PouchdbAdapterIdb);
    }

    if (isDevMode()) {
      await Promise.all([
        import('rxdb/plugins/dev-mode').then(
          (module) => addRxPlugin(module),
        ),
        import('rxdb/plugins/validate').then(
          (module) => addRxPlugin(module),
        ),
      ]);
    } else {
      addRxPlugin(RxDBNoValidatePlugin);
    }
  }
}
