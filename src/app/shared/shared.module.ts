import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HbpService } from './services/hbp.service';

export function initHbp(hbpService: HbpService) {
  return () => hbpService.init();
}

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  exports: [
  ],
  providers: [
    HbpService,
    {
      provide: APP_INITIALIZER,
      useFactory: initHbp,
      deps: [HbpService],
      multi: true,
    },
  ],
})
export class SharedModule { }
