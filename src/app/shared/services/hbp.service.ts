import { Injectable } from '@angular/core';
import { createClient, NhostClient } from 'nhost-js-sdk';
import Auth from 'nhost-js-sdk/dist/Auth';
import Storage from 'nhost-js-sdk/dist/Storage';

@Injectable({
  providedIn: 'root',
})
export class HbpService {
  nhostClient: NhostClient;
  auth: Auth;
  storage: Storage;

  init() {
    this.nhostClient = createClient({
      baseURL: 'http://localhost:3000',
    });

    this.auth = this.nhostClient.auth;
    this.storage = this.nhostClient.storage;
  }

  get isAuthenticated() {
    return this.auth.isAuthenticated();
  }

  get jwt() {
    return this.auth.getJWTToken();
  }

  get claim() {
    return this.auth.getClaim('x-hasura-user-id');
  }

  register(email, password) {
    return this.auth.register({ email, password });
  }

  login(email, password) {
    return this.auth.login({ email, password });
  }

  uploadFile(path, file, metadata?, onUploadProgress?) {
    return this.storage.put(path, file);
  }
}
