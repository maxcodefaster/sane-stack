import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {AppSettingsProperty} from './app-settings-property';

@Injectable({
  providedIn: 'root'
})
export class AppSettingsService {

  constructor(private storageProvider: Storage) {
  }

  userSettings = {
    uuid: new AppSettingsProperty('', 'uuid', this.storageProvider),
    userName: new AppSettingsProperty('', 'userName', this.storageProvider),
    introDone: new AppSettingsProperty(false, 'introDone', this.storageProvider),
  };

  getAppSettingsProperty<T, K extends keyof T>(model: T, key: K) {
    return model[key];
  }
}
