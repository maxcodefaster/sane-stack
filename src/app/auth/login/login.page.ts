import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HbpService } from '../../shared/services/hbp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(
    private router: Router,
    private hbp: HbpService,
  ) { }

  ngOnInit() {
  }

  login(form) {
    console.log(form.value);
    this.hbp.login(form.value.email, form.value.password).then((res) => {
      console.log(res);
    });
  }
}
