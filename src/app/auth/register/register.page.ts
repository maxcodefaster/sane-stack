import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HbpService } from '../../shared/services/hbp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  constructor(
    private hbp: HbpService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  register(form) {
    this.hbp.register(form.value.email, form.value.password);
  }
}
