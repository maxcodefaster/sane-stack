# The sane stack for app development

Fast cross-plattform apps with offline database sync via GraphQL Endpoint.

<u>FrontEnd:</u> 
- Ionic Angular Capacitor for scalable enterprise cross-plattform apps
- RxDB offline DB with SQLite & IDB Adapter
- Scully SSR for a SEO friendly and fast web app

<u>Backend:</u>
- Easy deployment with one command using docker-compose
- Postgresql
- Hasura GraphQL Engine
- Hasura Backend Plus for authentication and storage
- Use own monolith, microservices or serverless backend with hasura triggers
- All open source and scalable af

[[_TOC_]]

## Installation

### Requirements

- nodejs
- docker-compose

### Front End:

`npm i`

To run ionic app:

`ionic serve`

### Backend

Edit in `.env`
```sh
HASURA_GRAPHQL_ADMIN_SECRET=hasura_admin_secret_junge
S3_SECRET_ACCESS_KEY=minio_access_key_junge
```

Run

```sh
sudo docker-compose up -d
```


## Deployment

### Client
#### Web

Run

`npm run jam-it`

##### PWA Config
Use [pwa-asset-generator](https://github.com/onderceylan/pwa-asset-generator) and 
[web-app-manifest-generator](https://app-manifest.firebaseapp.com/) to theme your app, and change your app name
in the `index.html` and `manifest.webmanifest` .

#### Android & iOS
Change your app name and id in `capacitor.config.json` and `ionic.config.json` located in the root folder.
For compiling to native please refer to the official [capacitor documentation](https://capacitorjs.
com/docs/basics/building-your-app).

### Backend

Change env vars.

## Roadmap

- [X] GraphQL Replication
- [X] Secure backend with HasuraBackendPlus
- [X] Auth guards and service
- [X] Scully SSR
- [X] Simpler backend setup
- [ ] Better UI & UX on client demo app
- [ ] Implement https://github.com/Quramy/ts-graphql-plugin
